#
# Makefile
#

MAINPDFFILE=rust_wasm
EXAMPLEFILES=$(wildcard examples/*.tex)
EXAMPLEPDFFILES=$(patsubst %.tex,%.pdf,$(EXAMPLEFILES))
LATEX_COMPILER=xelatex -shell-escape

all: $(MAINPDFFILE).pdf clean

examples/%.pdf: examples/%.tex
	$(LATEX_COMPILER) -output-directory=examples $<

$(MAINPDFFILE).pdf: $(MAINPDFFILE).tex $(EXAMPLEPDFFILES) lug.cls
	$(LATEX_COMPILER) $<

clean:
	rm -f *.log *.aux *.log *.out *.bbl *.blg *.vrb *.snm *.toc

.PHONY: all clean
